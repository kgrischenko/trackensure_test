//
//  BorderedTextField.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

class BorderedTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 4.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        let spacerView = UIView(frame:CGRect(x: 0, y: 0, width: 10, height: 10))
        self.leftViewMode = .always
        self.leftView = spacerView
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

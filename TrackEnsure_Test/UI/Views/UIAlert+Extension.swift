//
//  UIAlert+Extension.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func emptyFields() -> UIAlertController {
        let vcAlert = UIAlertController(title: nil, message: "Please, fill all fields", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        vcAlert.addAction(okAction)
        
        return vcAlert
    }
    
    static func withMessage(_ message: String) -> UIAlertController {
        let vcAlert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        vcAlert.addAction(okAction)
        
        return vcAlert
    }
}

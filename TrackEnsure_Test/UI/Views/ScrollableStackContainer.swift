//
//  ScrollableStackContainer.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

class ScrollableStackContainer: UIScrollView {

    let svVertical = UIStackView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        self.backgroundColor = .gray
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
    }
    
    // MARK: - Public methods

    public func setup() {
        self.addSubview(svVertical)
        
        svVertical.autoPinEdgesToSuperviewEdges()
        svVertical.autoMatch(.height, to: .height, of: self)
        
        svVertical.axis = .vertical
        svVertical.alignment = .fill
        svVertical.distribution = .fillEqually
        svVertical.spacing = 8.0
    }
    
    public func addArrangedSubview(view: UIView) {
        svVertical.addArrangedSubview(view)
    }
    
}

//
//  RefillBaseViewController.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 30.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

class RefillBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(databaseDidUpdateHandler), name: .DatabaseDidUpdate, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Overridden

    public func updateView() {}
    
    // MARK: - Handlers

    @objc private func databaseDidUpdateHandler() {
        self.updateView()
    }

}

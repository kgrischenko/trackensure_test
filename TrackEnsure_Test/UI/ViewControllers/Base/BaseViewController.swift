//
//  BaseViewController.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 26.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

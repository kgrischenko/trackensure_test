//
//  GasStationsViewController.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit
import PureLayout

//если я правильно понял, «Заправки» - это действия пополнения топлива, а не заправочные станции
class RefillsViewController: RefillBaseViewController {
    
    lazy var tvRefills: UITableView = {
        let tv = UITableView()
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()
    
    private let cellReuseIdentifier = NSStringFromClass(RefillsViewController.self)
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(tvRefills)
        tvRefills.autoPinEdgesToSuperviewEdges()
        
        self.updateView()
    }
    
    override func updateView() {
        tvRefills.reloadData()
    }

}

// MARK: - UITableViewDataSource

extension RefillsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirebaseHelper.shared.refillsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var refillCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)
        if refillCell == nil {
            refillCell = UITableViewCell(style: .value1, reuseIdentifier: cellReuseIdentifier)
        }
        
        let refill = FirebaseHelper.shared.refill(at: indexPath.row)
        refillCell?.textLabel?.text = "\(refill?.provider ?? "") - \(refill?.address ?? ""), fuel: \(refill?.fuel ?? ""), \(refill?.cost ?? 0)$, \(refill?.quantity ?? 0)L"
        
        return refillCell!
    }    
}

// MARK: - UITableViewDelegate

extension RefillsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let refill = FirebaseHelper.shared.refill(at: indexPath.row) {
            let vcNewStation = NewRefillViewController(withMode: .edit, refill: refill)
            let vcNewStationNavigation = UINavigationController(rootViewController: vcNewStation)
            self.navigationController?.present(vcNewStationNavigation, animated: true, completion: nil)
        } else {
            preconditionFailure("refill for removing is not found")
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            FirebaseHelper.shared.delete(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)

        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
}

//
//  ContainerViewController.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 26.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit
import PureLayout

class ContainerViewController: BaseViewController {
    
    private lazy var scStations: UISegmentedControl = {
        let sc = UISegmentedControl()
        sc.insertSegment(withTitle: "Refills", at: 0, animated: false)
        sc.insertSegment(withTitle: "Statistic", at: 1, animated: false)
        sc.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        sc.selectedSegmentIndex = 0

        return sc
    }()
    
    private lazy var vcGasStations: UIViewController = {
        let vc = RefillsViewController()
        
        self.add(asChildViewController: vc)

        return vc
    }()
    
    private lazy var vcStationsStatistic: UIViewController = {
        let vc = StationsStatisticViewController()
        
        self.add(asChildViewController: vc)

        return vc
    }()

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = scStations
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewRefill))

        updateView()
    }
    
    // MARK: - Handlers

    @objc private func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
    
    @objc private func addNewRefill(_ sender: UIBarButtonItem) {
        let vcNewStation = NewRefillViewController(withMode: .new)
        let vcNewStationNavigation = UINavigationController(rootViewController: vcNewStation)
        self.navigationController?.present(vcNewStationNavigation, animated: true, completion: nil)
    }

    // MARK: - Helpers

    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)

        view.addSubview(viewController.view)

        viewController.view.autoPinEdgesToSuperviewSafeArea()

        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)

        viewController.view.removeFromSuperview()

        viewController.removeFromParent()
    }
    
    private func updateView() {
        if scStations.selectedSegmentIndex == 0 {
            remove(asChildViewController: vcStationsStatistic)
            add(asChildViewController: vcGasStations)
        } else {
            remove(asChildViewController: vcGasStations)
            add(asChildViewController: vcStationsStatistic)
        }
    }
}

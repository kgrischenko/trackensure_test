//
//  StatisticTableViewCell.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

class StatisticTableViewCell: UITableViewCell {
    
    @IBOutlet private var lblStationName: UILabel!
    @IBOutlet private var lblRefillTotalCount: UILabel!
    @IBOutlet private var lblMoneyTotalCount: UILabel!
    @IBOutlet private var lblLitersTotalCount: UILabel!
    @IBOutlet private var lblAddress: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func configure(with item: StationStatisticItem) {
        lblStationName.text = item.stationName
        lblRefillTotalCount.text = "\(item.totalRefillCount)"
        lblMoneyTotalCount.text = "\(item.totalMoneyCount)"
        lblLitersTotalCount.text = "\(item.totalFuelquantity)"
        lblAddress.text = " - \(item.address)"
    }
}

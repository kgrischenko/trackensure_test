//
//  StationsStatisticViewController.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

// сделано в виде примера, эту модель статистики можно как угодно улучшать и разширять

class StationsStatisticViewController: RefillBaseViewController {

    private lazy var tvStatistic: UITableView = {
        let tv = UITableView()
        tv.allowsSelection = false
        tv.dataSource = self
        tv.register(UINib(nibName: "StatisticTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        return tv
    }()
    
    private let cellReuseIdentifier = "StatisticTableViewCell"

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(tvStatistic)
        tvStatistic.autoPinEdgesToSuperviewEdges()
        
        self.updateView()
    }
    
    override func updateView() {
        tvStatistic.reloadData()
    }
}

// MARK: - UITableViewDataSource

extension StationsStatisticViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirebaseHelper.shared.stationsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let statisticCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! StatisticTableViewCell

        if let statisticItem = FirebaseHelper.shared.statisticItem(at: indexPath.row) {
            statisticCell.configure(with: statisticItem)
        }
        
        return statisticCell
    }
}

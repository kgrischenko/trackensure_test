//
//  NewStationViewController.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit
import MapKit

class NewRefillViewController: BaseViewController {
    
    let locationManager = CLLocationManager()
    
    var currentLocation: CLLocation? {
        didSet {
            lblLocation.text = (currentLocation != nil) ? "Latitude: \(currentLocation!.coordinate.latitude), longitude: \(currentLocation!.coordinate.longitude)" : "No location specified"
        }
    }
    
    lazy var mvStationCoordinate: MKMapView = {
        let mv = MKMapView(forAutoLayout: ())
        mv.showsUserLocation = true
        return mv
    }()
    
    lazy var tfProvider: UITextField = {
        let tf = BorderedTextField()
        tf.placeholder = "Provider"
        tf.delegate = self
        return tf
    }()
    
    lazy var tfQuantity: UITextField = {
        let tf = BorderedTextField()
        tf.keyboardType = .decimalPad
        tf.placeholder = "Quantity"
        tf.delegate = self
        return tf
    }()
    
    lazy var tfCost: UITextField = {
        let tf = BorderedTextField()
        tf.keyboardType = .decimalPad
        tf.placeholder = "Cost"
        tf.delegate = self
        return tf
    }()
    
    lazy var tfFueltype: UITextField = {
        let tf = BorderedTextField()
        tf.placeholder = "Type of fuel"
        tf.delegate = self
        return tf
    }()
    
    lazy var lblLocation: UILabel = {
        let lbl = UILabel()
        lbl.text = "No location specified"
        lbl.numberOfLines = 0
        return lbl
    }()

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "New Station"
        self.view.backgroundColor = .white
                
        self.view.addSubview(tfProvider)
        self.view.addSubview(tfFueltype)
        self.view.addSubview(tfQuantity)
        self.view.addSubview(tfCost)
        self.view.addSubview(lblLocation)

        tfProvider.autoPinEdge(toSuperviewSafeArea: .top, withInset: 12.0)
        tfProvider.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfProvider.autoAlignAxis(toSuperviewAxis: .vertical)
        tfProvider.autoSetDimension(.height, toSize: 32.0)
        
        tfFueltype.autoPinEdge(.top, to: .bottom, of: tfProvider, withOffset: 8.0)
        tfFueltype.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfFueltype.autoAlignAxis(toSuperviewAxis: .vertical)
        tfFueltype.autoSetDimension(.height, toSize: 32.0)

        tfQuantity.autoPinEdge(.top, to: .bottom, of: tfFueltype, withOffset: 8.0)
        tfQuantity.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfQuantity.autoAlignAxis(toSuperviewAxis: .vertical)
        tfQuantity.autoSetDimension(.height, toSize: 32.0)

        tfCost.autoPinEdge(.top, to: .bottom, of: tfQuantity, withOffset: 8.0)
        tfCost.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfCost.autoAlignAxis(toSuperviewAxis: .vertical)
        tfCost.autoSetDimension(.height, toSize: 32.0)
        
        lblLocation.autoPinEdge(.top, to: .bottom, of: tfCost, withOffset: 8.0)
        lblLocation.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        lblLocation.autoAlignAxis(toSuperviewAxis: .vertical)
        lblLocation.autoSetDimension(.height, toSize: 32.0)

        self.view.addSubview(mvStationCoordinate)
        mvStationCoordinate.autoPinEdge(.top, to: .bottom, of: lblLocation, withOffset: 8.0)
        mvStationCoordinate.autoPinEdge(toSuperviewEdge: .leading)
        mvStationCoordinate.autoPinEdge(toSuperviewEdge: .trailing)
        mvStationCoordinate.autoMatch(.height, to: .width, of: mvStationCoordinate)

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    deinit {
        print("deinit")
    }
    
    // MARK: - Handlers
    
    @objc private func saveActionDidTouchUp(_ sender: UIBarButtonItem) {
        
    }
    
    // MARK: - Helpers

    func validateEnteredData() {
        guard let provider = tfProvider.text, provider.trimmingCharacters(in: .whitespaces).count > 0 else {
            self.navigationItem.rightBarButtonItem = nil
            return
        }
        
        guard let quantity = tfQuantity.text, let floatQuantity = Float(quantity), floatQuantity > 0 else {
            self.navigationItem.rightBarButtonItem = nil
            return
        }
        
        guard let fuel = tfFueltype.text, fuel.trimmingCharacters(in: .whitespaces).count > 0 else {
            self.navigationItem.rightBarButtonItem = nil
            return
        }
        
        guard let cost = tfCost.text, let floatCost = Float(cost), floatCost > 0 else {
            self.navigationItem.rightBarButtonItem = nil
            return
        }
        
        guard let _ = currentLocation else {
            self.navigationItem.rightBarButtonItem = nil
            return
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveActionDidTouchUp))
    }
}

extension NewRefillViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        self.currentLocation = location
        
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        mvStationCoordinate.setRegion(coordinateRegion, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}


extension NewRefillViewController: UITextFieldDelegate {
    
}

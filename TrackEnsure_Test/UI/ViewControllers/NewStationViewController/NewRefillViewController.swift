//
//  NewStationViewController.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit
import MapKit

typealias RefillData = (provider: String, fuelType: String, quantity: Float, cost: Float, latitude: Double, longitude: Double, address: String)
typealias ValidationResult = Result<RefillData, ValidationError>

public enum ValidationError: Error {
    case emptyField
}


class NewRefillViewController: BaseViewController {
    
    private let locationManager = CLLocationManager()
    private let geoCoder = CLGeocoder()
    private var refill: Refill?
    private let mode: RefillEditingMode

    private var locationDescription: String?
    private var currentLocation: CLLocation? {
        didSet {
            if let location = currentLocation {
                updateMapView(with: location)
            }
        }
    }
    
    init(withMode mode: RefillEditingMode, refill: Refill? = nil) {
        self.mode = mode
        self.refill = refill

        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var mvStationCoordinate: MKMapView = {
        let mv = MKMapView(forAutoLayout: ())
        mv.showsUserLocation = true
        mv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(mapViewDidTap)))
        return mv
    }()
    
    private lazy var tfProvider: UITextField = {
        let tf = BorderedTextField()
        tf.placeholder = "Provider"
        tf.delegate = self
        tf.returnKeyType = .continue
        return tf
    }()
    
    private lazy var tfFueltype: UITextField = {
        let tf = BorderedTextField()
        tf.placeholder = "Type of fuel"
        tf.delegate = self
        tf.returnKeyType = .continue
        return tf
    }()
    
    private lazy var tfQuantity: UITextField = {
        let tf = BorderedTextField()
        tf.keyboardType = .decimalPad
        tf.placeholder = "Quantity"
        tf.delegate = self
        tf.returnKeyType = .continue
        return tf
    }()
    
    private lazy var tfCost: UITextField = {
        let tf = BorderedTextField()
        tf.keyboardType = .decimalPad
        tf.placeholder = "Cost"
        tf.delegate = self
        tf.returnKeyType = .done
        return tf
    }()
    
    private lazy var lblLocation: UILabel = {
        let lbl = UILabel()
        lbl.text = "No location specified"
        lbl.numberOfLines = 0
        return lbl
    }()

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = self.mode == .new ? "New Refill" : "Editing"
        self.view.backgroundColor = .white
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveActionDidTouchUp))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelActionDidTouchUp))

        self.view.addSubview(tfProvider)
        self.view.addSubview(tfFueltype)
        self.view.addSubview(tfQuantity)
        self.view.addSubview(tfCost)
        self.view.addSubview(lblLocation)

        tfProvider.autoPinEdge(toSuperviewSafeArea: .top, withInset: 12.0)
        tfProvider.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfProvider.autoAlignAxis(toSuperviewAxis: .vertical)
        tfProvider.autoSetDimension(.height, toSize: 32.0)
        
        tfFueltype.autoPinEdge(.top, to: .bottom, of: tfProvider, withOffset: 8.0)
        tfFueltype.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfFueltype.autoAlignAxis(toSuperviewAxis: .vertical)
        tfFueltype.autoSetDimension(.height, toSize: 32.0)

        tfQuantity.autoPinEdge(.top, to: .bottom, of: tfFueltype, withOffset: 8.0)
        tfQuantity.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfQuantity.autoAlignAxis(toSuperviewAxis: .vertical)
        tfQuantity.autoSetDimension(.height, toSize: 32.0)

        tfCost.autoPinEdge(.top, to: .bottom, of: tfQuantity, withOffset: 8.0)
        tfCost.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        tfCost.autoAlignAxis(toSuperviewAxis: .vertical)
        tfCost.autoSetDimension(.height, toSize: 32.0)
        
        lblLocation.autoPinEdge(.top, to: .bottom, of: tfCost, withOffset: 8.0)
        lblLocation.autoMatch(.width, to: .width, of: self.view, withOffset: -8.0)
        lblLocation.autoAlignAxis(toSuperviewAxis: .vertical)

        self.view.addSubview(mvStationCoordinate)
        mvStationCoordinate.autoPinEdge(.top, to: .bottom, of: lblLocation, withOffset: 8.0)
        mvStationCoordinate.autoPinEdge(toSuperviewEdge: .leading)
        mvStationCoordinate.autoPinEdge(toSuperviewEdge: .trailing)
        mvStationCoordinate.autoMatch(.height, to: .width, of: mvStationCoordinate)

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if let refill = self.refill, self.mode == .edit {
            tfProvider.text = refill.provider
            tfFueltype.text = refill.fuel
            tfQuantity.text = "\(refill.quantity)"
            tfCost.text = "\(refill.cost)"
            currentLocation = CLLocation(latitude: refill.latitude, longitude: refill.longitude)
        }
    }
    
    deinit {
        print("deinit")
    }
    
    // MARK: - Handlers
    
    @objc private func saveActionDidTouchUp() {
        switch validateEnteredData() {
        case .failure( _):
            self.navigationController?.present(UIAlertController.emptyFields(), animated: true, completion: nil)
        case .success(let refill):
            self.tryToSaveOrShowError(with: refill)
        }
    }
    
    @objc private func cancelActionDidTouchUp() {
        self.closeSelf()
    }
    
    @objc private func mapViewDidTap(_ tap: UITapGestureRecognizer) {
        if tap.state == .ended {
            let tappedLocation = self.mvStationCoordinate.convert(tap.location(in: self.mvStationCoordinate), toCoordinateFrom: self.mvStationCoordinate)
            self.currentLocation = CLLocation(latitude: tappedLocation.latitude, longitude: tappedLocation.longitude)
        }
    }
    
    // MARK: - Helpers
    
    func tryToSaveOrShowError(with refillData: RefillData) {
        do {
            if let refill = self.refill, self.mode == .edit {
                try FirebaseHelper.shared.editRefill(refill.id, with: refillData)
            } else {
                try FirebaseHelper.shared.saveNewRefill(with: refillData)
            }
        } catch let error as NSError {
            self.navigationController?.present(UIAlertController.withMessage(error.localizedDescription),
                                               animated: true,
                                               completion: nil)
            return
        }
        
        closeSelf()
    }

    func validateEnteredData() -> ValidationResult {
        guard let provider = tfProvider.text, provider.trimmingCharacters(in: .whitespaces).count > 0 else {
            return ValidationResult.failure(.emptyField)
        }
        
        guard let quantity = tfQuantity.text, let floatQuantity = Float(quantity), floatQuantity > 0 else {
            return ValidationResult.failure(.emptyField)
        }
        
        guard let fuel = tfFueltype.text, fuel.trimmingCharacters(in: .whitespaces).count > 0 else {
            return ValidationResult.failure(.emptyField)
        }
        
        guard let cost = tfCost.text, let floatCost = Float(cost), floatCost > 0 else {
            return ValidationResult.failure(.emptyField)
        }
        
        guard let location = currentLocation else {
            return ValidationResult.failure(.emptyField)
        }
        
        let address: String
        if let locationDescription = locationDescription {
            address = locationDescription
        } else {
            address = "\(location.coordinate.latitude), \(location.coordinate.longitude)"
        }
                
        return ValidationResult.success((provider: provider, fuel, floatQuantity, floatCost, location.coordinate.latitude, location.coordinate.longitude, address))
    }
    
    private func closeSelf() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    private func updateMapView(with location: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        mvStationCoordinate.setRegion(coordinateRegion, animated: true)
        
        let locationText = (currentLocation != nil) ? "Latitude: \(currentLocation!.coordinate.latitude), longitude: \(currentLocation!.coordinate.longitude)" : "No location specified"
        lblLocation.text = locationText
        
        self.geoCoder.reverseGeocodeLocation(location) { [weak self] placemarks, error in
            if let place = placemarks?.first {
                var address = ""
                if let thoroughfare = place.thoroughfare {
                    address += thoroughfare
                }
                if let subThoroughfare = place.subThoroughfare {
                    address += address.isEmpty ? subThoroughfare : " \(subThoroughfare)"
                }
                if let locality = place.locality {
                    address += address.isEmpty ? locality : ", \(locality)"
                }
                self?.locationDescription = address
                self?.lblLocation.text = locationText + "\n\(address)"
            }
        }
    }
}

extension NewRefillViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        if self.mode == .new {
            self.currentLocation = location
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}


extension NewRefillViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfProvider {
            tfFueltype.becomeFirstResponder()
        } else if textField == tfFueltype {
            tfQuantity.becomeFirstResponder()
        } else if textField == tfQuantity {
            tfCost.becomeFirstResponder()
        } else {
            saveActionDidTouchUp()
        }
        return true
    }
}

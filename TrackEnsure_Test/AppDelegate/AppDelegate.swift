//
//  AppDelegate.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 26.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  
        FirebaseApp.configure()
        
        return true
    }

}


//
//  DatabaseHelper.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit
import RealmSwift

final class DatabaseHelper: NSObject {
    
    static private let realm = try! Realm()
    
    static private var refillsCache = [Refill]()
    static private var refillMapper = RefillsMapper(with: [])
    
    static private let token = realm.observe { notification, realm in
        reloadCache()
    }

    // MARK: - Public

    static public func saveNewRefill(with data: RefillData) throws {
        
        let refill = Refill()
        refill.provider = data.provider
        refill.fuel = data.fuelType
        refill.quantity = data.quantity
        refill.cost = data.cost
        refill.latitude = data.latitude
        refill.longitude = data.longitude
        refill.address = data.address
        
        try realm.write {
            realm.add(refill)
        }
        
        DispatchQueue.main.async {
            NotificationCenter.default.post(Notification(name: .DatabaseDidUpdate))
        }
        
        SynchronizationService.shared.send(toServer: [refill]) { result in
            switch result {
            case .failure( _):
                // write failured refills and try to send them later
                let record = SynchronizationWaitingRecord(value: ["refillId" : refill.id])
                try? self.saveSynchronization(waiting: [record])
            default: break
            }
        }
    }
    
    static public func editRefill(_ refill: Refill, with data: RefillData) throws {
        try realm.write {
            refill.provider = data.provider
            refill.fuel = data.fuelType
            refill.quantity = data.quantity
            refill.cost = data.cost
            refill.latitude = data.latitude
            refill.longitude = data.longitude
            refill.address = data.address
        }
        
        DispatchQueue.main.async {
            NotificationCenter.default.post(Notification(name: .DatabaseDidUpdate))
        }
    }
    
    static public func refillsCount() -> Int {
        return refillsCache.count
    }
    
    static public func refill(at index: Int) -> Refill? {
        return refillsCache[index]
    }
    
    static public func delete(refill: Refill) {
        do {
            try realm.write {
                realm.delete(refill)
            }
        } catch let error as NSError {
            preconditionFailure(error.localizedDescription)
        }
    }
    
    static public func stationsCount() -> Int {
        return self.refillMapper.stationsCount()
    }
    
    static public func statisticItem(at index: Int) -> StationStatisticItem? {
        return self.refillMapper.statisticItem(at: index)
    }
    
    static public func saveSynchronization(waiting records: [SynchronizationWaitingRecord]) throws {
        try realm.write {
            realm.add(records)
        }
    }
    
    static public func removeSynchronization(waiting records: [SynchronizationWaitingRecord]) throws {
        try realm.write {
            realm.delete(records)
        }
    }
    
    static public func getAllSynchronizationRecords() -> [SynchronizationWaitingRecord] {
        return realm.objects(SynchronizationWaitingRecord.self).map { $0 }
    }
    
    static public func getRefill(by id: String) -> Refill? {
        return realm.object(ofType: Refill.self, forPrimaryKey: id)
    }
    
    // MARK: - Private

    static private func reloadCache() {
        self.refillsCache = realm.objects(Refill.self).sorted(byKeyPath: "date", ascending: false).map({ $0 })
        self.refillMapper = RefillsMapper(with: refillsCache)
    }
    
}

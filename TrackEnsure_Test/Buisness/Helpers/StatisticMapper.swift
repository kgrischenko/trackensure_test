//
//  StatisticMapper.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 30.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import UIKit

class RefillsMapper: NSObject {

    private var refills: [Refill]
    
    init(with refills: [Refill]) {
        self.refills = refills
    }
    
    public func statisticItem(at index: Int) -> StationStatisticItem? {
        let res = [String : [Refill]]()
        let staticticMap = refills.reduce(into: res) { (dic, refill) in
            let array = dic[refill.address, default: []]
            dic[refill.address] = array + [refill]
        }.sorted { $0.key < $1.key }
        
        let item = staticticMap[index]
        
        // totalMoneyCount paid on this gas station
        let totalMoneyCount = item.value.reduce(into: 0) { total, refill in
            total += refill.cost
        }
        // totalFuelquantity taken on this gas station
        let totalFuelQuantity = item.value.reduce(into: 0) { total, refill in
            total += refill.quantity
        }
        
        return StationStatisticItem(stationName: item.value.first?.provider ?? "",
                                    totalRefillCount: item.value.count,
                                    totalMoneyCount: totalMoneyCount,
                                    totalFuelquantity: totalFuelQuantity,
                                    address: item.key)
    }
    
    public func stationsCount() -> Int {
        let res = [String : [Refill]]()
        let stationsMap = refills.reduce(into: res) { (dic, refill) in
            let array = dic[refill.address, default: []]
            dic[refill.address] = array + [refill]
        }
        
        return stationsMap.count
    }
    
    public func refillsCount() -> Int {
        return self.refills.count
    }
    
    public func refill(at index: Int) -> Refill? {
        return self.refills[index]
    }
    
    public func remove(at index: Int) {
        self.refills.remove(at: index)
    }
}

//
//  Firebase.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 30.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import Firebase

class FirebaseHelper {

    private init() {
        
        Database.database().isPersistenceEnabled = true
        
        fireDatabase = Database.database().reference()
        
        refillsMapper = RefillsMapper(with: [])
        
        retriveRefills()
    }
    
    public static let shared = FirebaseHelper()

    private let fireDatabase: DatabaseReference
    
    private var refillsMapper: RefillsMapper

    // MARK: - Public

    public func saveNewRefill(with data: RefillData) throws {
        
        let refillDict: [String : Any] = [ "provider" : data.provider,
                                           "fuel" : data.fuelType,
                                           "quantity" : data.quantity as NSNumber,
                                           "cost" : data.cost as NSNumber,
                                           "latitude" : data.latitude as NSNumber,
                                           "longitude" : data.longitude as NSNumber,
                                           "address" : data.address
                                         ]
        self.userRefillsCollection().childByAutoId().setValue(refillDict)
    }
    
    public func editRefill(_ refillId: String, with data: RefillData) throws {
        let refillDict: [String : Any] = [ "provider" : data.provider,
                                           "fuel" : data.fuelType,
                                           "quantity" : data.quantity as NSNumber,
                                           "cost" : data.cost as NSNumber,
                                           "latitude" : data.latitude as NSNumber,
                                           "longitude" : data.longitude as NSNumber,
                                           "address" : data.address
                                         ]
        self.userRefillsCollection().child(refillId).updateChildValues(refillDict)
    }
    
    public func retriveRefills() {
        let _ = self.userRefillsCollection().observe(DataEventType.value, with: { (snapshot) in
            let refillsDict = snapshot.value as? [String : AnyObject] ?? [:]
            let refills = refillsDict.map { obj -> Refill in
                
                var value = obj.value as! [String : Any]
                value["id"] = obj.key
                return Refill(value: value)
            }
            
            self.refillsMapper = RefillsMapper(with: refills)

            DispatchQueue.main.async {
                NotificationCenter.default.post(Notification(name: .DatabaseDidUpdate))
            }
        })
    }
    
    public func delete(at index: Int) {
        guard let refill = self.refillsMapper.refill(at: index) else {
            return
        }
        self.refillsMapper.remove(at: index)
        self.userRefillsCollection().child(refill.id).removeValue()
    }
    
    public func stationsCount() -> Int {
        return self.refillsMapper.stationsCount()
    }
    
    public func statisticItem(at index: Int) -> StationStatisticItem? {
        return self.refillsMapper.statisticItem(at: index)
    }
    
    public func refillsCount() -> Int {
        return self.refillsMapper.refillsCount()
    }
    
    public func refill(at index: Int) -> Refill? {
        return self.refillsMapper.refill(at: index)
    }
    
    // MARK: - Private
    
    private enum Key {
        enum Collection {
            static let refills = "refills"
            static let users = "users"
        }
        
        enum User {
            static let name = "Mr Travolta"
        }
    }
    
    private func userRefillsCollection() -> DatabaseReference {
        return self.fireDatabase.child(Key.Collection.users).child(Key.User.name).child(Key.Collection.refills)
    }
}

//
//  SynchronizationService.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 29.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import Foundation
import Moya
import Alamofire

class SynchronizationService {
    
    // MARK: - Init

    private init() {
        
        isReachable = reachabilityManager?.isReachable ?? false
        
        reachabilityManager?.startListening(onUpdatePerforming: { [unowned self] status in
            switch status {
            case .notReachable, .unknown:
                self.isReachable = false
            case .reachable(.ethernetOrWiFi), .reachable(.cellular):
                self.isReachable = true
            }
        })
    }

    static let shared = SynchronizationService()
    
    private let provider = MoyaProvider<Synchronization>()
    
    private let reachabilityManager = Alamofire.NetworkReachabilityManager.default
    private var isReachable = false {
        didSet {
            if oldValue == false && isReachable == true {
                self.syncAllWaitingRecordsIfNeeded()
            }
        }
    }
    
    // MARK: - Public
    
    func send(toServer refills:[Refill], completion: @escaping (Result<Response, MoyaError>) -> Void) {
        provider.request(.refills(refills)) { result in
            completion(result)
        }
    }
    
    func syncAllWaitingRecordsIfNeeded() {
        let records = DatabaseHelper.getAllSynchronizationRecords()
        let refills = records.compactMap { synchronizationRecord -> Refill? in
            return DatabaseHelper.getRefill(by: synchronizationRecord.refillId)
        }
        
        if refills.count > 0 {
            self.send(toServer: refills) { result in
                switch result {
                case .success( _):
                    try? DatabaseHelper.removeSynchronization(waiting: records)
                default: break
                }
            }
        }
    }
}

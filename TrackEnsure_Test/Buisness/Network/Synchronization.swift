//
//  Sunchronization.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 29.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import Foundation
import Moya

enum Synchronization {
    case refills([Refill])
}

extension Synchronization: TargetType {
    
    public var baseURL: URL {
        return URL(string: "https://trackunsure.com/v1")!
    }
    
    public var path: String {
        switch self {
        case .refills: return "/refills"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .refills: return .post
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .refills(let refills):
            let refillsArray = refills.map { (refill) -> [String : String] in
                return  ["privider" : refill.provider,
                         "fuelType" : refill.fuel,
                         "quantity" : "\(refill.quantity)",
                         "cost" : "\(refill.cost)",
                         "latitude" : "\(refill.latitude)",
                         "longitude" : "\(refill.longitude)",
                         "address" : refill.address]
            }
            let params = ["refills" : refillsArray]
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        }
    }
    
    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}

//
//  Constants.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 29.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import Foundation


public enum RefillEditingMode {
    case new
    case edit
}

public extension Notification.Name {
    static let DatabaseDidUpdate = Notification.Name("DatabaseDidUpdate")
}

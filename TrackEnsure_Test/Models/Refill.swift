//
//  GasStation.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 27.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import Foundation
import RealmSwift

final class Refill: Object {

    @objc dynamic var id = UUID().uuidString
    @objc dynamic var provider = ""
    @objc dynamic var fuel = ""
    @objc dynamic var quantity: Float = 0.0
    @objc dynamic var cost: Float = 0.0
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    @objc dynamic var date = Date()
    @objc dynamic var address: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}


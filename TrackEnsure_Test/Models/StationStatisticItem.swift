//
//  StationStatisticItem.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 28.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import Foundation

class StationStatisticItem: NSObject {

    let stationName: String
    let totalRefillCount: Int
    let totalFuelquantity: Float
    let totalMoneyCount: Float
    let address: String

    init(stationName: String, totalRefillCount: Int, totalMoneyCount: Float, totalFuelquantity: Float, address: String) {
        self.stationName = stationName
        self.totalRefillCount = totalRefillCount
        self.totalMoneyCount = totalMoneyCount
        self.totalFuelquantity = totalFuelquantity
        self.address = address
    }
    
}

//
//  SynchronizationWaitingRecord.swift
//  TrackEnsure_Test
//
//  Created by Grishchenko Konstantin on 29.06.2020.
//  Copyright © 2020 Grishchenko Konstantin. All rights reserved.
//

import Foundation
import RealmSwift

final class SynchronizationWaitingRecord: Object {
    @objc dynamic var refillId = ""
}
